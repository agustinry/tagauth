import wx
import serial
import logging
import os
import sys
import configparser
from threading import Thread
from wx.lib.pubsub import pub

import locator
import log
import handler_archivos as fh

class filethread(Thread):

    def __init__(self):

        Thread.__init__(self)

        log.loggingInit('rfid.log')

        #obtener los parametros del archivo de inicializacion
        cfgfile_path = locator.config_path('init.cfg')
        config = configparser.ConfigParser()
        config.read(cfgfile_path)

        self.validadores_path = config.get('paths','validadores_path')
        self.cant_lectores = config.getint('lectores','cant_lectores')

        self.modo = config.getint('firmware','version')

    def run(self):

        self.nuevo_uid = False
        has_auth = False
        surt_id = -1
        uid = -1

        while(self.is_running):

            if(self.nuevo_uid is True):

                if(self.modo == 1):
                    surt_id, uid, vbat = self.uid_raw.split(',')
                    vbat = vbat.rstrip()
                    log.writeLog('Nuevo tag presente SURT_ID = %s - TAG = %s' % (surt_id, uid), 'main.filethread', 'info')
                    log.writeLog('Nivel de bateria SURT_ID = %s - %s' % (surt_id, vbat), 'main.filethread', 'info')
                else:
                    surt_id, uid = self.uid_raw.split(',')
                    uid = uid.rstrip()
                    log.writeLog('Nuevo tag presente SURT_ID = %s - TAG = %s' % (surt_id, uid), 'main.filethread', 'info')

                self.no_despachado = True
                self.nuevo_uid = False
                #reiniciar el archivo de auth para asegurarse que no haya
                #ninguna autorizacion previa ni nada
                fh.escribir_archivo(self.validadores_path, 'auth' + surt_id + '.txt', '')

                #escribir el tag
                fh.escribir_uid(self.validadores_path, surt_id, uid)

                #leer el resultado de la autorizacion
                while(self.no_despachado):

                    linea = fh.leer_autorizacion(self.validadores_path, surt_id)

                    #autorizacion
                    if(linea.find('AUTH') != -1):

                        uid_autorizado = linea[-2:-1]

                        if(uid_autorizado == '1'):
                            log.writeLog('SURT_ID = %s con TAG = %s autorizado' % (surt_id, uid), 'main.filethread', 'info')
                            pub.sendMessage('auth.result', result=1, surt_id=surt_id)
                            has_auth = True
                        else:
                            log.writeLog('SURT_ID = %s con TAG = %s NO autorizado' % (surt_id, uid), 'main.filethread', 'info')
                            #salir del loop porque no se autorizo el tag
                            has_auth = False
                            self.no_despachado = False
                            pub.sendMessage('auth.result', result=0, surt_id=surt_id)
                            fh.escribir_archivo(self.validadores_path, 'tag'+surt_id+'.txt','$'+surt_id+',NOTAG')

                        #reiniciar el archivo de auth
                        fh.escribir_archivo(self.validadores_path, 'auth' + surt_id + '.txt', '')

                    #despacho
                    if(linea.find('DESP') != -1 and has_auth == True):
                        despacho = linea[-2:-1]

                        if(despacho == '1'):
                            log.writeLog('SURT_ID = %s con TAG = %s despachado' % (surt_id, uid), 'main.filethread', 'info')
                            self.no_despachado = False
                            has_auth = False
                            pub.sendMessage('auth.result', result=2, surt_id=surt_id)
                            #reiniciar auth
                            fh.escribir_archivo(self.validadores_path, 'auth' + surt_id + '.txt', '')
                            fh.escribir_archivo(self.validadores_path, 'tag'+surt_id+'.txt','$'+surt_id+',NOTAG')

                #end while no despachado
            #end if nuevo_uid
        #end while is_running

        log.writeLog('Filethread service stopped for SURT_ID = %s - TAG = %s' % (surt_id, uid), 'main.filethread', 'debug')

    def stop(self):
        #salir de todos los loops
        self.no_despachado = False
        self.is_running = False

    def t_start(self):
        self.is_running = True
        self.no_despachado = True
        self.start()

    def update_uid(self, uid_raw):
        self.nuevo_uid = True
        self.uid_raw = uid_raw
