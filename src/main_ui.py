import wx
import locator

class main_ui(wx.Panel):

    def __init__(self, parent, *args, **kwargs):
        wx.Panel.__init__(self, parent, *args, **kwargs)

        self.DEFAULT_COLOR = wx.Colour(100,100,100)
        self.BG_COLOR = wx.Colour(255,255,255)
        self.SetBackgroundColour(self.BG_COLOR)

        """Definicion de sizers para el Layout"""
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        userSizer = wx.FlexGridSizer(rows=1,cols=3,hgap=5,vgap=5)

        """widgets"""
        loginHeader = wx.Bitmap(locator.resource_path('login.png'), wx.BITMAP_TYPE_PNG)
        loginHeaderSB = wx.StaticBitmap(self, bitmap=loginHeader)

        self.btnConectar = wx.Button(self, label="Conectar", size=(-1,-1))
        self.btnCerrar = wx.Button(self, label="Cerrar", size=(-1,-1))

        self.btnActualizar = wx.Button(self, label="Actualizar", size=(-1,-1))
        #self.btnExaminarUID = wx.Button(self, label="Examinar...", size=(-1,-1))
        #self.btnExaminarAuth = wx.Button(self, label="Examinar...", size=(-1,-1))

        self.choicePuerto = wx.Choice( self, size=(110,-1) )
        self.choicePuerto.SetSelection(0)

        self.labelPuerto = wx.StaticText(self,label=u"Puerto")
        #self.labelUID = wx.StaticText(self,label=u"UID file", size=(-1,-1))
        #self.labelAuth = wx.StaticText(self,label=u"AUTH file", size=(-1,-1))

        self.labelLeyendoDatos = wx.StaticText(self, label=u"Leyendo datos del receptor...")
        self.labelLeyendoDatos.SetForegroundColour(self.BG_COLOR)

        #self.editUID = wx.TextCtrl(self)
        #self.editUID.SetEditable(False)
        #self.editAuth = wx.TextCtrl(self)
        #self.editAuth.SetEditable(False)

        """Set sizers"""
        userSizer.AddMany([
            (self.labelPuerto,0,wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL|wx.ALL,5),
            (self.choicePuerto,1,wx.ALIGN_CENTER|wx.ALL|wx.EXPAND,5),
            (self.btnActualizar,0,wx.ALIGN_CENTER|wx.ALL,5)
            #(self.labelUID,0,wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL|wx.ALL,5),
            #(self.editUID,1,wx.ALIGN_CENTER|wx.ALL|wx.EXPAND,5),
            #(self.btnExaminarUID,0,wx.ALIGN_CENTER|wx.ALL,5),
            #(self.labelAuth,0,wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL|wx.ALL,5),
            #(self.editAuth,1,wx.ALIGN_CENTER|wx.ALL|wx.EXPAND,5),
            #(self.btnExaminarAuth,0,wx.ALIGN_CENTER|wx.ALL,5)
        ])

        btnSizer.Add(self.btnConectar, 0, wx.ALL, 5)
        btnSizer.Add(self.btnCerrar, 0, wx.ALL, 5)

        mainSizer.Add(loginHeaderSB, 0, wx.ALL|wx.ALIGN_CENTER, 5)
        mainSizer.Add((-1,20))
        mainSizer.Add(userSizer, 0, wx.ALL|wx.ALIGN_CENTER, 5)
        mainSizer.Add(self.labelLeyendoDatos, 0, wx.ALL|wx.ALIGN_CENTER, 5)
        mainSizer.Add((-1,10))
        mainSizer.Add(btnSizer, 0, wx.ALL|wx.ALIGN_CENTER, 5)

        self.SetSizer(mainSizer)
