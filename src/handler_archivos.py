import wx
import os

def escribir_archivo(path, filename, data):
    fpath = os.path.join(path,filename)
    try:
        with open(fpath, 'w') as file:
            strn_to_save = data + '\n'
            file.write(strn_to_save)
    except IOError:
        pass

def leer_autorizacion(path, surt_id):
    filename = 'auth'+str(surt_id)+'.txt'
    path = os.path.join(path,filename)
    try:
        with open(path, 'r') as file:
            auth_line = file.readline()
    except IOError:
        pass

    return auth_line

def escribir_uid(path, surt_id, uid):
    filename = 'tag'+str(surt_id)+'.txt'
    path = os.path.join(path,filename)
    try:
        with open(path, 'w') as file:
            strn_to_save = '$' + str(surt_id) + ',' + uid + '\n'
            file.write(strn_to_save)
    except IOError:
        pass
