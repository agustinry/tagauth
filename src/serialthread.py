import wx
import serial
import logging
import os
import configparser
from threading import Thread
from wx.lib.pubsub import pub

import locator
import log
import handler_archivos as fh
from filethread import filethread

class serialthread(Thread):

    def __init__(self, serial_port):

        Thread.__init__(self)

        self.is_running = True

        #obtener los parametros del puerto serie del archivo de inicializacion
        cfgfile_path = locator.config_path('init.cfg')
        config = configparser.ConfigParser()
        config.read(cfgfile_path)

        log.loggingInit('rfid.log')
        log.writeLog("Serial service started", 'main.serialthread', 'info')

        serial_port_cfg = config.get('serial','port')
        self.validadores_path = config.get('paths','validadores_path')
        self.cant_lectores = config.getint('lectores','cant_lectores')

        self.modo = config.getint('firmware','version')

        self.serial = serial.Serial()

        if(serial_port_cfg == 'AUTO'):
            self.serial.port = serial_port
            self.serial.baudrate = 9600
            self.serial.timeout = 3
        else:
            self.serial.port = serial_port_cfg
            self.serial.baudrate = config.getint('serial','baudrate')
            self.serial.timeout = config.getint('serial','timeout')

        try:
            self.serial.open()
            self.start()
        except Exception as e:
            strn = 'Error al conectar: %s' % e
            log.error("%s" % strn)
            wx.MessageBox(strn, 'Atencion', wx.OK | wx.ICON_ERROR)

        #reiniciar los archivos del sistema de validadores
        for i in range(self.cant_lectores):
            fh.escribir_archivo(self.validadores_path,'tag'+str(i)+'.txt','$'+str(i)+',NOTAG')
            fh.escribir_archivo(self.validadores_path,'auth'+str(i)+'.txt','')

        self.fth = [filethread() for i in range(self.cant_lectores)]

        #iniciar los threads de los archivos
        for t in self.fth:
            t.t_start()

        pub.subscribe(self.authResult, 'auth.result')

    def run(self):

        data = []
        nuevo_uid = False

        while(self.is_running):

            #leo la data del uid que viene del puerto serie
            for c in self.serial.read():
                data.append(chr(c)) #convert from ASCII
                uid_raw = ''.join(str(v) for v in data) #Make a string from array

                if chr(c) == '\n':
                    data = []
                    nuevo_uid = True
                    break

            #cada vez que hay un nuevo uid desde el serial
            if(nuevo_uid is True):
                if(self.modo==1):
                    surt_id, uid, vbat = uid_raw.split(',')
                else:
                    surt_id, uid = uid_raw.split(',')
                    
                nuevo_uid = False

                #actualizar el thread correspondiente
                self.fth[int(surt_id)].update_uid(uid_raw)

        #cerrar el puerto serie cuando termino el thread
        self.serial.close()

    def authResult(self, result, surt_id):
        #no autorizado
        if(result == 0):
            strn = "N," + surt_id + '\n'
        #autorizado
        elif(result == 1):
            strn = "A," + surt_id + '\n'
        #despachado
        elif(result == 2):
            strn = "D," + surt_id + '\n'

        self.serial.write(strn.encode())

    def stop(self):
        #salir de todos los loops
        self.no_despachado = False
        self.is_running = False

        #terminar todos los threads de archivos
        for ft in self.fth:
            ft.stop()
