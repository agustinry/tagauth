import logging

def loggingInit(logFile):
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - [%(name)s] - %(levelname)s - %(message)s',
                        filename=logFile
                        )

def writeLog(string, logger, level='info'):
    log = logging.getLogger(logger)

    file_logging_level_switch = {
        'debug':    log.debug,
        'info':     log.info,
        'warning':  log.warning,
        'error':    log.error,
        'critical': log.critical
    }

    file_logging_level_switch[level](string)
