import os,sys

def we_are_frozen():
    # All of the modules are built-in to the interpreter, e.g., by py2exe
    return hasattr(sys, "frozen")

def module_path():
    #python2.7
    #encoding = sys.getfilesystemencoding()
    if we_are_frozen():
        return os.path.dirname(str(sys.executable))
    return os.path.dirname(str(__file__))

def resource_path(png_name):
    #python2.7
    #encoding = sys.getfilesystemencoding()
    if we_are_frozen():
        current_dir = os.path.dirname(str(sys.executable))
    else:
        current_dir = os.path.dirname(str(__file__))

    res_path = os.path.abspath(os.path.join(current_dir,os.pardir))
    res_path = os.path.join(res_path,'res')
    #res_path = os.path.join(current_dir,'res')
    res_path = os.path.join(res_path,png_name)

    return res_path

def config_path(cfg_name):
    #python2.7
    #encoding = sys.getfilesystemencoding()
    if we_are_frozen():
        current_dir = os.path.dirname(str(sys.executable))
    else:
        current_dir = os.path.dirname(str(__file__))

    config_path = os.path.abspath(os.path.join(current_dir,os.pardir))
    config_path = os.path.join(config_path,cfg_name)

    return os.path.normpath(config_path)
