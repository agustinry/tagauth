from cx_Freeze import setup, Executable

target = Executable(
    script="RFID.py",
    base="Win32GUI",
    icon="icon.ico"
    )

setup(
    name="Lector TAG RFID",
    version="0.1",
    executables=[target]
    )
