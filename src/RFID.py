import wx
import serial
import glob
import sys
import os
import logging
import configparser

import locator
import log
from main_ui import main_ui
from serialthread import serialthread


class main_frame(wx.Frame):
    """Frame principal de la interfaz."""

    def __init__(self, parent):
        """Inicializacion de UI y objeto serial."""
        wx.Frame.__init__(self, parent, wx.ID_ANY, "Lector RFID v0.1",
                          size=(400, 320), style=wx.DEFAULT_FRAME_STYLE &
                          (~wx.RESIZE_BORDER))

        self.is_connected = False
        self.sthread = None

        #obtener los parametros del puerto serie del archivo de inicializacion
        cfgfile_path = locator.config_path('init.cfg')
        config = configparser.ConfigParser()
        config.read(cfgfile_path)
        self.serial_port_cfg = config.get('serial','port')

        self.init_user_interface()

        log.loggingInit('rfid.log')
        log.writeLog('Program started', 'main', 'info')

        if(self.serial_port_cfg == 'AUTO'):
            log.writeLog("Serial port is set to AUTO", 'main', 'info')
        else:
            log.writeLog("Serial port is set to %s" % self.serial_port_cfg, 'main', 'info')

    def init_user_interface(self):
        """Inicializacion de los paneles de interfaz de usuario."""

        ico = wx.Icon(locator.resource_path('icon.png'), wx.BITMAP_TYPE_PNG)
        self.SetIcon(ico)

        self.mainPanel = main_ui(self)
        self.actualizar_serie()

        self.mainPanel.btnActualizar.Bind(wx.EVT_BUTTON, self.actualizar)
        self.mainPanel.btnConectar.Bind(wx.EVT_BUTTON, self.btn_conectar)
        self.mainPanel.btnCerrar.Bind(wx.EVT_BUTTON, self.cerrar)

        #catch para cuando apreta el boton de cerrar de la ventana
        self.Bind(wx.EVT_CLOSE, self.close_evt)

        #conectar automaticamente al primer puerto que aparece
        self.conectar()

    def btn_conectar(self, evt):
        self.conectar()

    def conectar(self):

        index = self.mainPanel.choicePuerto.GetSelection()
        serial_port = self.mainPanel.choicePuerto.GetString(index)

        if(serial_port == ''):
            log.writeLog("Serial port not found", 'main', 'info')
            wx.MessageBox('No se encontro ningun puerto serie',
                          'Atencion', wx.OK | wx.ICON_INFORMATION)
            return

        if(self.is_connected is not True):
            self.sthread = serialthread(serial_port)
            self.mainPanel.labelLeyendoDatos.SetForegroundColour((0,102,0))
            self.is_connected = True
            self.mainPanel.btnConectar.SetLabel(u"Detener")
            self.mainPanel.Layout()
        else:
            self.mainPanel.labelLeyendoDatos.SetForegroundColour((255,255,255))
            self.is_connected = False
            self.mainPanel.btnConectar.SetLabel(u"Conectar")
            self.mainPanel.Layout()
            self.sthread.stop()

    def actualizar(self, evt):
        self.actualizar_serie()

    def actualizar_serie(self):
        if(self.serial_port_cfg == 'AUTO'):
            self.mainPanel.choicePuerto.Clear()

            #ignorar los primeros dos puertos (range(3,256))
            #son ocupados por los puertos fisicos de los surtidores o telemedidor
            if sys.platform.startswith('win'):
                ports = ['COM%s' % (i + 1) for i in range(2,256)]
            elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
                ports = glob.glob('/dev/tty[A-Za-z]*')
            elif sys.platform.startswith('darwin'):
                ports = glob.glob('/dev/tty.*')
            else:
                raise EnvironmentError('Unsupported platform')

            for port in ports:
                try:
                    s = serial.Serial(port)
                    s.close()
                    self.mainPanel.choicePuerto.Append(port)
                except (OSError, serial.SerialException):
                    pass

            if(len(ports)!=0):
                self.mainPanel.choicePuerto.SetSelection(0)

        else:
            self.mainPanel.btnActualizar.Enable(False)
            self.mainPanel.choicePuerto.Append(self.serial_port_cfg)
            self.mainPanel.choicePuerto.SetSelection(0)

    def close_evt(self, evt):
        self.cerrar_app()

    def cerrar(self, evt):
        self.cerrar_app()

    def cerrar_app(self):
        if(self.sthread is not None and self.sthread.is_running):
            self.sthread.stop()
        self.Destroy()

class MyApp(wx.App):
    """
    inicializacion de la clase MyApp de tipo wx.App.

    Se llama automaticamente al __init__ de la
    superclase wx.App
    """

    def __init__(self, redirect):
        wx.App.__init__(self, redirect)

    # este metodo se llama cuando se crea la app (equivalente al init) antes
    # del MainLoop
    def OnInit(self):
        frame = main_frame(None)
        # frame.ShowFullScreen(True, style=wx.FULLSCREEN_ALL)
        frame.Show()
        return True

app = MyApp(False)
app.MainLoop()
